#!/usr/bin/python3

import sys
import pygame
import time
import os

pygame.init()
pygame.font.init()

if os.environ['DISPLAY'] == "":
    # Run full-screen if we're not under X
    INITIAL_SCREEN_WIDTH=0
    INITIAL_SCREEN_HEIGHT=0
else:
    # Run in a window under X
    INITIAL_SCREEN_WIDTH=1360
    INITIAL_SCREEN_HEIGHT=768

size = width, height = INITIAL_SCREEN_WIDTH, INITIAL_SCREEN_HEIGHT
speed = [0, 0]
black = 0, 0, 0
grey = 40, 40, 40

# Set up window
pygame.display.set_caption('md5sumchecker')
if os.environ['DISPLAY'] == "":
    screen = pygame.display.set_mode((size), pygame.FULLSCREEN)
    pygame.mouse.set_visible(False)
else:
    screen = pygame.display.set_mode((size), pygame.NOFRAME)
screen.fill(grey)

SCREEN_WIDTH, SCREEN_HEIGHT = pygame.display.get_surface().get_size()

# Load images
usbimg = pygame.image.load('/usr/share/md5sumchecker/usb.png')
logoimg = pygame.image.load('/usr/share/md5sumchecker/aims-desktop.png')


def clear_screen():
    screen.fill(grey)


def drawcard_intro():
    clear_screen()
    pygame.draw.rect(screen, (180, 38, 34), (50, 50, SCREEN_WIDTH-110, 110))
    titlefont = pygame.font.SysFont("Cantarell Extra Bold", 120)
    label = titlefont.render("AIMS Desktop", 1, (255, 255, 255))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 105))
    img_rect = logoimg.get_rect(center=(SCREEN_WIDTH/2, 105))
    screen.blit(logoimg, img_rect)

    textfont = pygame.font.SysFont("Cantarell Bold", 40)
    label = textfont.render("This tool will check the integrity of files on this installation media.",
                            1, (200, 200, 200))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 240))
    screen.blit(label, text_rect)

    textfont = pygame.font.SysFont("Cantarell Bold", 40)
    label = textfont.render("To continue press space. To quit press ESC.", 1, (200, 200, 200))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 330))
    screen.blit(label, text_rect)

    img_rect = usbimg.get_rect(center=(SCREEN_WIDTH/2, 500))
    screen.blit(usbimg, img_rect)

    pygame.display.flip()


def drawcard_letswork():
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if (event.key == pygame.K_ESCAPE):
                    print("Escape pressed, exiting...")
                    drawcard_bye()
                    sys.exit()
        clear_screen()
        pygame.draw.rect(screen, (40, 80, 80), (50, 50, SCREEN_WIDTH-110, 110))
        myfont = pygame.font.SysFont("Cantarell Extra Bold", 120)
        label = myfont.render("Testing media", 1, (255, 255, 255))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 105))
        screen.blit(label, text_rect)
        pygame.display.flip()

        md5sums = [line.rstrip('\n') for line in open('md5sum.txt')]
        md5sum_count = (len(md5sums))

        textfont = pygame.font.SysFont("Cantarell Bold", 40)
        label = textfont.render("Found %s checksums that need to be checked." % md5sum_count,
                                1, (200, 200, 200))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 240))
        screen.blit(label, text_rect)
        pygame.display.flip()
        time.sleep(2)

        checked_count = 0
        for entry in md5sums:
            checked_count = checked_count + 1
            checksum = entry.split(' ')[0]
            filename = entry.split(' ')[2]

            checkedsum = os.popen('md5sum ' + filename).read()
            print(checkedsum.split(' ')[0])

            if checksum == checkedsum.split(' ')[0]:
                print("Checksum for file %s passed" % filename)
            else:
                print("Checksum for file %s failed" % filename)
                drawcard_report(filename)

            textfont = pygame.font.SysFont("Cantarell Bold", 40)
            label = textfont.render("%s files left to check...." % str(md5sum_count-checked_count) ,
                                    1, (200, 200, 200))
            phonylabel = textfont.render("9999 files left to check...." ,
                                         1, (200, 200, 200))
            text_rect = phonylabel.get_rect(center=(SCREEN_WIDTH/2, 340))
            pygame.draw.rect(screen, (grey), phonylabel.get_rect(center=(SCREEN_WIDTH/2, 340)))
            screen.blit(label, text_rect)
            pygame.display.flip()

            if checked_count == md5sum_count:
                time.sleep(1)
                print("we're done!")
                drawcard_report('success')


def drawcard_bye():
    clear_screen()
    pygame.draw.rect(screen, (180, 38, 34), (50, 50, SCREEN_WIDTH-110, 110))
    myfont = pygame.font.SysFont("Cantarell Extra Bold", 120)
    label = myfont.render("Good bye!", 1, (255, 255, 255))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 105))
    screen.blit(label, text_rect)

    textfont = pygame.font.SysFont("Cantarell Bold", 40)
    label = textfont.render("Rebooting...",
                            1, (200, 200, 200))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2))
    screen.blit(label, text_rect)

    pygame.display.flip()
    time.sleep(1)


def drawcard_report(status):
    clear_screen()
    if status == "success":
        pygame.draw.rect(screen, (40, 100, 100), (50, 50, SCREEN_WIDTH-110, 110))
        myfont = pygame.font.SysFont("Cantarell Extra Bold", 120)
        label = myfont.render("Report: Success", 1, (255, 255, 255))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 105))
        screen.blit(label, text_rect)

        textfont = pygame.font.SysFont("Cantarell Bold", 40)
        label = textfont.render("Good news! Your installation media is in good shape.",
                                1, (200, 200, 200))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 340))
        screen.blit(label, text_rect)

        textfont = pygame.font.SysFont("Cantarell Bold", 40)
        label = textfont.render("Press any key to reboot.",
                                1, (200, 200, 200))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 440))
        screen.blit(label, text_rect)

        pygame.display.flip()

    else:
        print("Failed: " + status)
        pygame.draw.rect(screen, (220, 100, 100), (50, 50, SCREEN_WIDTH-110, 110))
        myfont = pygame.font.SysFont("Cantarell Extra Bold", 120)
        label = myfont.render("Report: Failure", 1, (255, 255, 255))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 105))
        screen.blit(label, text_rect)

        textfont = pygame.font.SysFont("Cantarell Bold", 40)
        label = textfont.render("Unfortunately, the recorded checksums didn't match what's on this media.",
                                1, (200, 200, 200))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 240))
        screen.blit(label, text_rect)

        textfont = pygame.font.SysFont("Cantarell Bold", 40)
        label = textfont.render("Verify your download or try another disk.",
                                1, (200, 200, 200))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 340))
        screen.blit(label, text_rect)

        textfont = pygame.font.SysFont("Cantarell Bold", 40)
        label = textfont.render("For help, visit https://desktop.aims.ac.za",
                                1, (200, 200, 200))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 440))
        screen.blit(label, text_rect)

        textfont = pygame.font.SysFont("Cantarell Bold", 40)
        label = textfont.render("Press any key to reboot...",
                                1, (200, 200, 200))
        text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 540))
        screen.blit(label, text_rect)

        screen.blit(label, text_rect)
        pygame.display.flip()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                drawcard_bye()
                sys.exit()


while True:
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if (event.key == pygame.K_ESCAPE):
                print("Escape pressed, exiting...")
                drawcard_bye()
                sys.exit()
            if (event.key == pygame.K_SPACE):
                print("Space pressed, let's draw work card...")
                drawcard_letswork()


        if event.type == pygame.QUIT:
            sys.exit()

    drawcard_intro()

